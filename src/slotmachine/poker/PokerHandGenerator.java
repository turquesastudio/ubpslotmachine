package slotmachine.poker;

import java.io.*;
import java.util.*;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.StandardOpenOption.*;

public class PokerHandGenerator {
    public Map<String, Integer> generate() {
        Map<String, Integer> handDictionary = new HashMap<>();
        Properties properties = new Properties();
        File file = new File("./resources/PokerHand.properties");

        try {
            properties.load(newInputStream(file.toPath(), READ));

            for (Object key : properties.keySet()) {
//                System.out.println(key.toString() + " " + properties.get(key));

                String[] handProperty = properties.get(key).toString().split("\\,");
                SortedSet<String> hand = new TreeSet<String>();

                for (String symbol : handProperty[0].split("\\|")) {
                    hand.add(symbol);
                }

                handDictionary.put(hand.toString(), Integer.parseInt(handProperty[1]));
            }

//            System.out.println(handDictionary);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return handDictionary;
    }
}
