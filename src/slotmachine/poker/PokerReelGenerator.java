package slotmachine.poker;

import slotmachine.core.reel.IReelSymbol;
import slotmachine.core.reel.Reel;
import slotmachine.core.reel.ReelSymbol;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.StandardOpenOption.READ;

public class PokerReelGenerator {
    public List<Reel> generate() {
        List<Reel> reels = new ArrayList<>();
        Properties properties = new Properties();
        File file = new File("./resources/PokerReels.properties");

        try {
            properties.load(newInputStream(file.toPath(), READ));

            for (Object key : properties.keySet()) {
//                System.out.println(key.toString() + " " + properties.get(key));

                List<IReelSymbol> reelSymbols = new ArrayList<>();

                for (String symbol : properties.get(key).toString().split("\\|")) {
                    reelSymbols.add(new ReelSymbol(symbol));
                }

                reels.add(new Reel(reelSymbols));
            }

//            System.out.println(reels);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return reels;
    }
}
