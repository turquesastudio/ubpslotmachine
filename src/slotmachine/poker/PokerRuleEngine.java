package slotmachine.poker;

import slotmachine.core.reel.Reel;
import slotmachine.core.ruleengine.IRuleEngine;

import java.util.*;

public class PokerRuleEngine implements IRuleEngine {
    private List<Reel> reels;
    private List<Integer> reelSize;
    private Map<String, Integer> handDictionary;

    public PokerRuleEngine(List<Reel> reels, Map<String, Integer> handDictionary) {
        this.reels = reels;
        this.handDictionary = handDictionary;

        reelSize = new ArrayList<>();

        for (Reel reel : reels) {
            reelSize.add(reel.getReelSize());
        }
    }

    @Override
    public int getBetResult(int bet) {
        SortedSet<String> matchResultHand = new TreeSet<>();

        for (Reel reel : reels) {
            matchResultHand.add(reel.currentSymbol().toString());
        }

        System.out.println(matchResultHand.toString());

        Integer handValue = handDictionary.get(matchResultHand.toString());

        return handValue != null ? handValue : 0;
    }

    @Override
    public List<Reel> getReels() {
        return reels;
    }

    @Override
    public List<Integer> getReelSize() {
        return reelSize;
    }
}