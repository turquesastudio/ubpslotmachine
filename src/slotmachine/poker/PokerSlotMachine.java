package slotmachine.poker;


import slotmachine.core.SlotMachine;
import slotmachine.core.credit.*;
import slotmachine.core.gamemode.GameMode;
import slotmachine.core.gamemode.GameModeFactory;
import slotmachine.core.gamemode.random.IRandomize;
import slotmachine.core.gamemode.random.RandomGameModeFactory;
import slotmachine.core.gamemode.random.RandomSequenceGameModeFactory;
import slotmachine.core.gamemode.random.Randomize;
import slotmachine.core.reel.Reel;
import slotmachine.core.ruleengine.IRuleEngine;
import slotmachine.ui.handler.ICreditHandler;
import slotmachine.ui.handler.IGameModeHandler;
import slotmachine.ui.handler.IPlayHandler;
import slotmachine.ui.view.IReelAdapter;
import slotmachine.ui.view.IView;
import slotmachine.ui.view.SlotMachineViewFacade;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PokerSlotMachine {
//    public static int credits = 0;

    public enum GameModes {
        RANDOM,
        RANDOM_SEQUENCE;
    }

    public static GameModes currentGameMode = GameModes.RANDOM;

    public static void main(String... args) {
//        SlotMachineViewFacade.setCreditHandler(new ICreditHandler() {
//            @Override
//            public void addCredit(ICredit credit) {
//                credits += credit.getValue();
//                SlotMachineViewFacade.getDisplayHandler().setText("Bet: " +credits);
//                SlotMachineViewFacade.setInputEnabled(true);
//            }
//        });
//        SlotMachineViewFacade.setPlayHandler(new IPlayHandler() {
//            @Override
//            public void play() {
//                SlotMachineViewFacade.getDisplayHandler().setText("Insert Credit");
//
//                SlotMachineViewFacade.setInputEnabled(false);
//
//                new java.util.Timer().schedule(
//                        new java.util.TimerTask() {
//                            @Override
//                            public void run() {
//                                SlotMachineViewFacade.getPrizeHandler().retrieve(credits);
//
//                                SlotMachineViewFacade.setCreditInputEnabled(true);
//
//                                credits = 0;
//                            }
//                        },
//                        5000
//                );
//            }
//        });
//
//        SlotMachineViewFacade.show();


        Map<String, Integer> handDictionary = new PokerHandGenerator().generate();
        List<Reel> reels = new PokerReelGenerator().generate();
        IRuleEngine ruleEngine = new PokerRuleEngine(reels, handDictionary);

        IRandomize randomize = new Randomize();
        GameMode gameMode = GameModeFactory.create(new RandomGameModeFactory(ruleEngine.getReelSize(), randomize));

        ICreditTranslator creditTranslator = new CreditTranslator();
        ICreditBox creditBox = new CreditBox(200);
        ICreditReceiver creditReceiver = new CreditReceiver(creditTranslator);
        ICreditIssuer creditIssuer = new CreditIssuer(creditTranslator);
        ICreditOrchestrator creditOrchestrator = new CreditOrchestrator(creditBox, creditReceiver, creditIssuer);

        ICreditBeneficiary creditBeneficiary = new ICreditBeneficiary() {
            @Override
            public void benefit(ICredit credit) {
                if (credit.getValue() > 0) {
                    SlotMachineViewFacade.getPrizeHandler().retrieve(credit.getValue());
                } else {
                    SlotMachineViewFacade.getDisplayHandler().setText("YOU LOSE!");
                }
            }

            @Override
            public ICredit currentFunds() {
                return new ICredit() {
                    @Override
                    public int getValue() {
                        return 0;
                    }
                };
            }
        };

        SlotMachine slotMachine = new SlotMachine(ruleEngine, gameMode, creditOrchestrator, creditBeneficiary);


        SlotMachineViewFacade.setCreditHandler(new ICreditHandler() {
            @Override
            public void addCredit(slotmachine.ui.data.ICredit credit) {
                creditOrchestrator.receiveCredit(new ICredit() {
                    @Override
                    public int getValue() {
                        return credit.getValue();
                    }
                });
                SlotMachineViewFacade.getDisplayHandler().setText("Bet: " +creditOrchestrator.currentBet());
                //SlotMachineViewFacade.setInputEnabled(true);
            }
        });
        SlotMachineViewFacade.setPlayHandler(new IPlayHandler() {
            @Override
            public void play() {
                creditOrchestrator.betCredit();
                slotMachine.play();
                SlotMachineViewFacade.getReelHandler().update();
            }
        });
        SlotMachineViewFacade.setGameModeHandler(new IGameModeHandler() {
            @Override
            public String change() {
                if (currentGameMode.equals(GameModes.RANDOM)) {
                    slotMachine.setGameMode(GameModeFactory.create(new RandomSequenceGameModeFactory(ruleEngine.getReelSize(), randomize, 10)));

                    currentGameMode = GameModes.RANDOM_SEQUENCE;

                    return GameModes.RANDOM.toString();
                } else {
                    slotMachine.setGameMode(GameModeFactory.create(new RandomGameModeFactory(ruleEngine.getReelSize(), randomize)));

                    currentGameMode = GameModes.RANDOM;

                    return GameModes.RANDOM_SEQUENCE.toString();
                }
            }
        });

        SlotMachineViewFacade.setReelAdapter(new IReelAdapter() {
            @Override
            public int getCount() {
                return reels.size();
            }

            @Override
            public IView getView(int position) {
                final JLabel label = new JLabel(reels.get(position).currentSymbol().toString());
                label.setHorizontalAlignment(SwingConstants.CENTER);

                return new IView() {
                    @Override
                    public Component getComponent() {
                        return label;
                    }
                };
            }
        });

        SlotMachineViewFacade.show();
    }
}
